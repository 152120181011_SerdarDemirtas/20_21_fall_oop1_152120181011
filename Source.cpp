#include <iostream>
#include <fstream>

using namespace std;

int main() {
	
	/** Variables definitions */
	int numbersofintegers = 0;
	int numbersarray[500] = { 0 };
	int i = 0;
	int sum = 0;
	int product = 1;
	float average = 0;
	int smallest = 0;

	/** File Reading part of the program */
	ifstream dosya("input.txt");

	dosya >> numbersofintegers;

	while (dosya) {
		dosya >> numbersarray[i];
		i++;
	}
	
	/** 4 operations of the program */

	/** Sum operation */
	for (int j = 0; j < numbersofintegers; j++) {
		sum = sum + numbersarray[j];
	}
	
	cout << "Sum is " << sum << endl;

	/** Product operation */
	for (int j = 0; j < numbersofintegers; j++) {
		product = product * numbersarray[j];
	}

	cout << "Product is " << product << endl;

	/** Average operation */
	average = (float)sum / (float)numbersofintegers;
	
	cout << "Average is " << average << endl;

	/** Finding smallest operation */

	smallest = numbersarray[0];

	for (int j = 0; j < numbersofintegers; j++) {
		if (numbersarray[j] < smallest) {
			smallest = numbersarray[j];
		}
	}

	cout << "Smallest is " << smallest << endl;

	return 0;
}